import React, { Component } from "react";
import Header from "./layouts/Header";
import Product from "./components/Product";
import FilterProducts from "./components/FilterProducts";
import SortByPrice from "./components/SortByPrice";
import Searchbar from "./components/Searchbar";
import Loader from "./components/Loader";
import ErrorPage from "./components/ErrorPage";
import Footer from "./layouts/Footer";
import "./App.css";

export default class App extends Component {
  // My State
  state = {
    products: [],
    originalProducts: [], // Maintain a copy of original products
    category: "All", // Filter By Category
    sortPrice: "none", // Sort Products
    searchValue: "", // Search Bar
    loading: true, // For Loader
    pageError: false, // Error Handling
  };

  // Loading The Api for the First Time and Storing in State
  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((data) =>
        this.setState({
          products: data,
          originalProducts: data,
          loading: false,
        })
      )
      .catch((err) => {
        this.setState({ pageError: true, loading: false });
      });
  }

  // Function to handle show details of each product
  handleClick = (id) => {
    const items = this.state.products;
    const itemIndex = items.findIndex((item) => {
      return item.id === id;
    });
    items[itemIndex].show = !items[itemIndex].show;
    this.setState({ products: items });
  };

  // Function to handle delete of each product
  handleDelete = (id) => {
    const items = this.state.products.filter((item) => {
      return item.id !== id;
    });
    this.setState({ products: items });
  };

  // Function to handle category change of products
  handleCategoryChange = (e) => {
    const category = e.target.value;

    const products = this.state.originalProducts.filter((product) => {
      if (product.category === category) {
        return product;
      } else if (category === "all") {
        return product;
      } else {
        return null;
      }
    });

    return this.setState({
      products: products,
      sortPrice: "none",
      searchValue: "",
      category: e.target.value,
    });
  };

  // Function to handle sort operations on products
  handleSortPrice = (e) => {
    const sort = e.target.value;
    const products = this.state.products.sort((productA, productB) => {
      if (sort === "ascending") {
        return productA.price - productB.price;
      } else if (sort === "descending") {
        return productB.price - productA.price;
      } else if (sort === "atoz") {
        return productA.title.localeCompare(productB.title);
      } else if (sort === "ztoa") {
        return productB.title.localeCompare(productA.title);
      } else if (sort === "rating-desc") {
        return productB.rating.rate - productA.rating.rate;
      } else if (sort === "rating-asc") {
        return productA.rating.rate - productB.rating.rate;
      } else {
        return null;
      }
    });
    this.setState({ products: products, sortPrice: sort });
  };

  // Function to handle search bar by title on products
  handleSearchChange = (e) => {
    const search = e.target.value.toLowerCase();
    let products;
    if (search === "") {
      products = [...this.state.originalProducts];
    } else {
      products = this.state.originalProducts.filter((product) => {
        return (
          product.title.toLowerCase().includes(search) ||
          product.category.toLowerCase().includes(search)
        );
      });
    }
    this.setState({
      products: products,
      searchValue: search,
      sortPrice: "none",
    });
  };

  // Render life cycle
  render() {
    const ProductList = this.state.products.map((product) => {
      return (
        <Product
          key={product.id}
          data={product}
          handleClick={this.handleClick}
          handleDelete={this.handleDelete}
        />
      );
    });
    return (
      <div className="main-body">
        <Header />
        <h1 className="main-body-title">LootKart App</h1>
        <div className="filter-sort">
          <FilterProducts
            selectedCategory={this.state.category}
            onCategoryChange={this.handleCategoryChange}
          />
          <Searchbar
            searchValue={this.state.searchValue}
            onSearchChange={this.handleSearchChange}
          />
          <SortByPrice
            sortPrice={this.state.sortPrice}
            onSortPrice={this.handleSortPrice}
          />
        </div>
        {this.state.pageError ? (
          <ErrorPage />
        ) : this.state.loading ? (
          <Loader />
        ) : (
          <main>{ProductList}</main>
        )}
        <Footer />
      </div>
    );
  }
}
