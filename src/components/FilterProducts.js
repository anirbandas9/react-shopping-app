import React from "react";

function FilterProducts({ selectedCategory, onCategoryChange }) {
  return (
    <div className="filter-container">
      <label htmlFor="category">Filter By Category: </label>
      <select
        id="category"
        name="category"
        value={selectedCategory}
        onChange={onCategoryChange}
      >
        <option value="all">All</option>
        <option value="men's clothing">Men's Clothing</option>
        <option value="women's clothing">Women's Clothing</option>
        <option value="jewelery">Jewelery</option>
        <option value="electronics">Electronics</option>
      </select>
    </div>
  );
}

export default FilterProducts;
