import React from "react";
import ProductBody from "./ProductBody";
import StarRating from "./StarRating";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-regular-svg-icons";

function Product({ data, handleClick, handleDelete }) {
  const { title, image, price, show, rating, description } = data;
  const showDesc = description.split(" ").slice(0, 3).join(" ");
  const showTitle = title.split(" ").slice(0, 6).join(" ");
  // const pricing = String(price).split('.');
  return (
    <div className="product">
      <div className="product-img">
        <FontAwesomeIcon icon={faHeart} className="heart-icon"/>
        <img className="img" src={image} alt="" />
      </div>
      <div className="product-title">
        <h2>{showTitle}</h2>

        <p>
          ${price}
          {/* <span style={{ fontWeight: "500" }}>${price}</span> */}
        </p>
      </div>
      <div className="product-desc">{showDesc}</div>
      <div className="div-rating">
        {/* <h4>Rating:</h4> */}
        <StarRating rating={rating.rate} maxRating={5} />
        <span className="rating-count">{`(${rating.count})`}</span>
      </div>
      <div className="product-btn">
        <button className="showDetailsBtn">Add to Cart</button>
        {/* <button className="showDetailsBtn" onClick={() => handleClick(id)}>
          {show ? "Hide Details" : "Show Details"}
        </button> */}
        {/* <button className="crossBtn" onClick={() => handleDelete(id)}>
          x
        </button> */}
      </div>
      {show ? <ProductBody description={description} /> : null}
    </div>
  );
}

export default Product;
