import React from "react";

function StarRating({ rating, maxRating }) {
  // Calculate the number of full stars
  const fullStars = Math.round(rating);

  // Create an array to store the stars
  const stars = [];

  // Create the full stars
  for (let i = 0; i < fullStars; i++) {
    stars.push(
      <span key={i} className="star">
        &#9733;
      </span>
    );
  }

  // Fill remaining empty stars
  for (let i = fullStars; i < maxRating; i++) {
    stars.push(
      <span key={i} className="star">
        &#9734;
      </span>
    );
  }
  return <div className="star-rating">{stars}</div>;
}

export default StarRating;
