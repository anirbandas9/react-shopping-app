import React from "react";

function ProductBody({ description }) {
  return (
    <div className="product-body">
      <p className="product-desc">{description}</p>
    </div>
  );
}

export default ProductBody;
