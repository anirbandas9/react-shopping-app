import React from "react";

function SortByPrice({ sortPrice, onSortPrice }) {
  return (
    <div className="sort-container">
      <label htmlFor="sort">Sort Products By: </label>
      <select name="sort" id="sort" value={sortPrice} onChange={onSortPrice}>
        <option value="none">Choose</option>
        <option value="ascending">Price Lowest First</option>
        <option value="descending">Price Highest First</option>
        <option value="rating-desc">Rating Highest First</option>
        <option value="rating-asc">Rating Lowest First</option>
        <option value="atoz">Sort By Title [A-Z]</option>
        <option value="ztoa">Sort By Title [Z-A]</option>
      </select>
    </div>
  );
}

export default SortByPrice;